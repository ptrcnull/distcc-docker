FROM alpine:edge

RUN apk upgrade -Ua
RUN apk add alpine-sdk distcc clang python3
RUN update-distcc-symlinks

# wow nice hack
RUN echo "::once:/entrypoint" > /etc/inittab
COPY entrypoint /

ENV DISTCC_ALLOW 0.0.0.0/0
ENV DISTCC_LISTEN 0.0.0.0
ENV DISTCC_JOBS 8

CMD ["/sbin/init"]
